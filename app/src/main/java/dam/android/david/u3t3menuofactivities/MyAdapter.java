package dam.android.david.u3t3menuofactivities;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class MyAdapter extends RecyclerView.Adapter<MyAdapter.MyViewHolder> {

    public interface onItemClickListener {
        void onItemClick(String ActivityName);
    }

    private final String[] myDataSet;
    private onItemClickListener listener;

    // Constructor para setear listData y el listener para onItemCLick
    public MyAdapter(String[] myDataSet, onItemClickListener listener) {
        this.myDataSet = myDataSet;
        // Setear listener para onItemCLick
        this.listener = listener;
    }

    // Clase para cada elemento de la lista. Solo contiene un TextView
    static class MyViewHolder extends RecyclerView.ViewHolder {
        TextView textView;

        public MyViewHolder(TextView textView) {
            super(textView);
            this.textView = textView;
        }
        // Llena el ViewHolder de datos

        public void bind(String activityName, onItemClickListener listener) {
            this.textView.setText(activityName);
            // Llama al listener cuando clickamos el textView
            this.textView.setOnClickListener(v -> listener.onItemClick(textView.getText().toString()));
        }
    }

    // Constructor para llenar el listado de datos
    MyAdapter(String[] myDataSet) {
        this.myDataSet = myDataSet;
    }

    // Crea un nuevo objeto view: Layout Manager llama a éste metodo.
    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        //Crear nuevo view
        //User un simple layout predefinido que solo contiene el TextView
        TextView tv = (TextView) LayoutInflater.from(parent.getContext())
                .inflate(android.R.layout.simple_list_item_1, parent, false);
        return new MyViewHolder(tv);
    }

    // Reemplaaza el contenido de datos del viewHolder: Layout Manager llama a este metodo
    @Override
    public void onBindViewHolder(@NonNull MyViewHolder viewHolder, int position) {
    // bind viewHolder con datos en: posicion
    // setear también el listener al viewHolder
        viewHolder.bind(myDataSet[position], listener);
    }

    // Devuelve el tamaño del DataSet: layout Manager llama a este método.
    @Override
    public int getItemCount() {
        return myDataSet.length;
    }
}
