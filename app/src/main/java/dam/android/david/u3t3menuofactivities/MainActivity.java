package dam.android.david.u3t3menuofactivities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity implements MyAdapter.onItemClickListener{

    private RecyclerView recyclerView;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager layoutManager;

    private String[] myDataset = {"Activity1", "Activity2", "Activity3"};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        setUI();
    }

    private void setUI() {

        recyclerView = findViewById(R.id.recyclerViewActivities);
        // Usa esta opción para mejorar el rendimiento si sabes que cambios en el contenido no cambian el tamaño del Layout
        recyclerView.setHasFixedSize(true);

        // Configurar Recyclerview con un LayoutManager
        layoutManager = new LinearLayoutManager(this, RecyclerView.VERTICAL, false);
        recyclerView.setLayoutManager(layoutManager);

        // Configurar un Adapter para Recyclerview
        mAdapter = new MyAdapter(myDataset, this);
        recyclerView.setAdapter(mAdapter);

    }

    @Override
    public void onItemClick(String activityName) {
        Toast.makeText(this, activityName, Toast.LENGTH_LONG).show();

        try {
            startActivity(new Intent(this, Class.forName(getPackageName() + "." + activityName)));
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }
}