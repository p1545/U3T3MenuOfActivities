package dam.android.david.u3t3menuofactivities;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import dam.android.david.u3t3menuofactivities.model.Item;

public class ItemDetailActivity extends AppCompatActivity {

    TextView nombreDetailTV, versionDetailTV, apiDetailTV, urlDetailTV;
    ImageView imageId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_item_detail);

        //TODO EX.1.3 Activamos el boton "Up" en la Action bar y personalizamos el título en el manifest
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        //TODO EX.2.4 Añadimos las imágenes en todas las densidades

        //TODO EX.2.3 Asignamos los datos para la versión seleccionada

        Item item = (Item) getIntent().getSerializableExtra("Item");
        imageId = findViewById(R.id.imageDetailTV);
        nombreDetailTV = findViewById(R.id.nombreDetailTV);
        versionDetailTV = findViewById(R.id.versionDetailTV);
        apiDetailTV = findViewById(R.id.apiDetailTV);

        imageId.setImageResource(item.getImageId());
        nombreDetailTV.setText(item.getName());
        versionDetailTV.setText(item.getVersion());
        apiDetailTV.setText(item.getApi());

        //TODO EX.2.6 Abrimos la URL al clicar en la imagen:.
        imageId.setOnClickListener(new View.OnClickListener() {

            public void onClick(View v) {
                Intent intent = new Intent();
                intent.setAction(Intent.ACTION_VIEW);
                intent.addCategory(Intent.CATEGORY_BROWSABLE);
                intent.setData(Uri.parse(item.getUrl()));
                startActivity(intent);
            }
        });
    }
}