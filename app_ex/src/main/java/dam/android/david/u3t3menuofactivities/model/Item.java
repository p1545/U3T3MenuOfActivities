package dam.android.david.u3t3menuofactivities.model;

import java.io.Serializable;

//TODO EX.2.2 Creamos la clase Item para recoger la información de cada versión de Android


public class Item implements Serializable {

    public int imageId;
    public String api;
    public String version;
    public String name;
    public String year;
    public String url;

    public Item() {
    }

    public Item(int imageId, String api, String version, String name, String year, String url) {
        this.imageId = imageId;
        this.api = api;
        this.version = version;
        this.name = name;
        this.year = year;
        this.url = url;

    }

    public int getImageId() {
        return imageId;
    }
    public void setImageId(int imageId) {
        this.imageId = imageId;
    }

    public String getApi() {
        return api;
    }
    public void setApi(String api) {
        this.api = api;
    }

    public String getVersion() {
        return version;
    }
    public void setVersion(String version) {
        this.version = version;
    }

    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }

    public String getYear() {
        return year;
    }
    public void setYear(String year) {
        this.year = year;
    }

    public String getUrl() {
        return url;
    }
    public void setUrl(String url) {
        this.url = url;
    }
}
