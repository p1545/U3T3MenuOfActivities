package dam.android.david.u3t3menuofactivities;

import android.content.Context;
import android.media.Image;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.view.menu.MenuView;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import dam.android.david.u3t3menuofactivities.model.Item;

class MyAdapter extends RecyclerView.Adapter<MyAdapter.MyViewHolder> {

    private ArrayList<Item> myDataSet = new ArrayList<>();
    private MyAdapter.onItemClickListener listener;
    private LayoutInflater mInflater;
    private Context context;

    public interface onItemClickListener {
        void onItemClick(Item item);
    }

    public MyAdapter(ArrayList<Item> myDataSet, Context context, MyAdapter.onItemClickListener listener) {
        this.myDataSet = myDataSet;
        this.listener = listener;
        this.mInflater = LayoutInflater.from(context);
        this.context = context;
    }

    //TODO EX.2.3 RecyclerView muestra toda la información en sus correspondientes CardViews

    static class MyViewHolder extends RecyclerView.ViewHolder {
        ImageView imageId;
        TextView api, name, version;
        CardView cardView;

        public MyViewHolder(View itemView) {
            super(itemView);
            imageId = itemView.findViewById(R.id.imageView);
            name = itemView.findViewById(R.id.nombreTV);
            version = itemView.findViewById(R.id.versionTV);
            api = itemView.findViewById(R.id.apiTV);
            cardView = itemView.findViewById(R.id.myCardView);
        }

        public void bind(Item item, onItemClickListener listener) {
            name.setText(item.getName());
            version.setText(item.getVersion());
            api.setText(item.getApi());
            imageId.setImageResource(item.getImageId());
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onItemClick(item);
                }
            });

        }
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) { //Metemos un cardview en lugar del textview

         View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_item, parent, false);
        return new MyAdapter.MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder viewHolder, int position) {
        viewHolder.cardView.setAnimation(AnimationUtils.loadAnimation(context, R.anim.fade_transition));
        viewHolder.bind(myDataSet.get(position), listener);
    }

    @Override
    public int getItemCount() {
        return myDataSet.size();
    }
}
