package dam.android.david.u3t3menuofactivities;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.widget.ListAdapter;
import android.widget.Toast;

import java.util.ArrayList;

import dam.android.david.u3t3menuofactivities.model.Item;

public class MainActivity extends AppCompatActivity {

    //TODO EX.2.1 Sustituimos el dataset por un ArrayList de Items
    ArrayList<Item> myDataset;

    private RecyclerView recyclerView;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager layoutManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        setUI();
    }

    private void setUI() {

        //TODO EX.2.2 Modificamos la aplicación para que recoga las 8 últimas versiones de Android

        myDataset = new ArrayList<>();
        myDataset.add(new Item (R.drawable.android_12_developer_preview_logo_svg, "31", "Versión: 12", "Android 12", "2021", "https://es.wikipedia.org/wiki/Android_12"));
        myDataset.add(new Item (R.drawable.android11, "30", "Versión: 11", "Android 11", "2020", "https://es.wikipedia.org/wiki/Android_11"));
        myDataset.add(new Item (R.drawable.android10, "29", "Versión: 10", "Android 10", "2019", "https://es.wikipedia.org/wiki/Android_10"));
        myDataset.add(new Item (R.drawable.pie, "28", "Versión: 9", "Pie", "2018", "https://es.wikipedia.org/wiki/Android_Pie"));
        myDataset.add(new Item (R.drawable.oreo, "26", "Versión: 8", "Oreo", "2017", "https://es.wikipedia.org/wiki/Android_Oreo"));
        myDataset.add(new Item (R.drawable.nougat, "24", "Versión: 7", "Nougat", "2016", "https://es.wikipedia.org/wiki/Android_Nougat"));
        myDataset.add(new Item (R.drawable.marshmallow, "23", "Versión: 6", "Marshmallow", "2015", "https://es.wikipedia.org/wiki/Android_Marshmallow"));
        myDataset.add(new Item (R.drawable.lollipop, "21", "Versión: 5", "Lollipop", "2014", "https://es.wikipedia.org/wiki/Android_Lollipop"));

        recyclerView = findViewById(R.id.recyclerViewActivities);
        recyclerView.setHasFixedSize(true);

        layoutManager = new LinearLayoutManager(this, RecyclerView.VERTICAL, false);
        recyclerView.setLayoutManager(layoutManager);


        //TODO EX.1.1 Redirigimos los items a una única activity (ItemDetalActivity)

        mAdapter = new MyAdapter(myDataset, this, new MyAdapter.onItemClickListener() {
            @Override
            public void onItemClick(Item item) {
                moveToItemDetail(item);
            }
        });

        //TODO EX.4.1.2 Implementamos el Swipe
        new ItemTouchHelper(itemTouchHelperCallback).attachToRecyclerView(recyclerView);
        recyclerView.setAdapter(mAdapter);
    }

    //TODO EX.1.2 Enviamos al intent la activity seleccionada y la mostramos desde ItemDetailActivity
    public void moveToItemDetail(Item item) {
        Toast.makeText(this, item.getName(), Toast.LENGTH_SHORT).show(); // Toast con nombre de versión
        Intent intent = new Intent(this, ItemDetailActivity.class); //Intent con el nombre de la Activity
        intent.putExtra("Item", item);
        startActivity(intent);
    }

    //TODO EX.4.1.1 Implementamos el Swipe

    ItemTouchHelper.SimpleCallback itemTouchHelperCallback = new ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.RIGHT) {
        @Override
        public boolean onMove(@NonNull RecyclerView recyclerView, @NonNull RecyclerView.ViewHolder viewHolder, @NonNull RecyclerView.ViewHolder target) {
            return false;
        }

        @Override
        public void onSwiped(@NonNull RecyclerView.ViewHolder viewHolder, int direction) {
        myDataset.remove(viewHolder.getAdapterPosition());
        mAdapter.notifyDataSetChanged();
        }
    };

}